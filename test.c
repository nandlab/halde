#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "halde.h"

#include <errno.h>
#include <stdint.h>

struct mblock {
    struct mblock *next;
    size_t size;
    char memory[];
};

int main(void) {
    int err = 0;

    printList();

    char *m1, *m2, *m3, *m4;

    m1 = malloc(200*1024);
    printList();
    if (!m1) {
        err = ENOMEM;
        goto myret;
    }
    m2 = malloc(250*1024);
    printList();
    if (!m2) {
        err = ENOMEM;
        goto free1;
    }
    m3 = malloc(100*1024);
    printList();
    if (!m3) {
        err = ENOMEM;
        goto free2;
    }
    m4 = malloc(300*1024);
    printList();
    if (!m4) {
        err = ENOMEM;
        goto free3;
    }

    free(m1);
    printList();
    free(m2);
    printList();
    free(m3);
    printList();
    free(m4);
    printList();

    m1 = malloc(100*1024);
    printList();
    if (!m1) {
        err = ENOMEM;
        goto myret;
    }
    m2 = malloc(110*1024);
    printList();
    if (!m2) {
        err = ENOMEM;
        goto free1;
    }
    m3 = malloc(120*1024);
    printList();
    if (!m3) {
        err = ENOMEM;
        goto free2;
    }
    m4 = malloc(130*1024);
    printList();
    if (!m4) {
        err = ENOMEM;
        goto free3;
    }

    void *p;

    // Allocating zero bytes should return implementation defined pointer
    // which should be freed
    p = malloc(0);
    printList();
    free(p);
    printList();

    // Allocating more than SIZE should not be possible.
    // A NULL pointer will be returned.
    p = malloc(1024*1024*2);
    printList();
    if (!p) {
        // In this case expected
    }
    else {
        // Unexpected
        err = 1;
        free(p);
        printList();
        goto free4;
    }

free4:
    free(m4);
    printList();
free3:
    free(m3);
    printList();
free2:
    free(m2);
    printList();
free1:
    free(m1);
    printList();
myret:
    printList();
    return err;
}

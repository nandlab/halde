#include "halde.h"
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <assert.h>
#include <stdbool.h>

/// Magic value for occupied memory chunks.
#define MAGIC ((void*)0xbaadf00d)

/// Size of the heap (in bytes).
#define SIZE (1024*1024*1)

/// Memory-chunk structure.
struct mblock {
	struct mblock *next;
	size_t size;
	char memory[];
};

/// Heap-memory area.
static char memory[SIZE];

/// Pointer to the first element of the free-memory list.
static struct mblock *head = NULL;

static bool memory_initialized = false;

/// Helper function to visualise the current state of the free-memory list.
void printList(void) {
	struct mblock *lauf = head;

	// Empty list
	if (head == NULL) {
		char empty[] = "(empty)\n";
        // Subtract one from the size because of the null terminator
        write(STDERR_FILENO, empty, sizeof(empty) - 1);
		return;
	}

	// Print each element in the list
	const char fmt_init[] = "(off: %7zu, size:: %7zu)";
	const char fmt_next[] = " --> (off: %7zu, size:: %7zu)";
	const char * fmt = fmt_init;
	char buffer[sizeof(fmt_next) + 2 * 7];

	while (lauf) {
		size_t n = snprintf(buffer, sizeof(buffer), fmt
			, (uintptr_t) lauf - (uintptr_t)memory, lauf->size);
		if (n) {
			write(STDERR_FILENO, buffer, n);
		}

		lauf = lauf->next;
		fmt = fmt_next;
	}
	write(STDERR_FILENO, "\n", 1);
}

#define MBLOCK_SIZE (sizeof(struct mblock))

#define mem2block(mem) ((struct mblock *) ((char *) (mem) - MBLOCK_SIZE))

/* inline struct mblock *mem2block(void *mem) {
    return (struct mblock *) ((char *) mem - MBLOCK_SIZE);
} */

void *malloc (size_t size) {

    // Initialization
    if (!memory_initialized) {
        assert(MBLOCK_SIZE < SIZE);
        head = (struct mblock *) memory;
        head->size = SIZE - MBLOCK_SIZE;
        head->next = (struct mblock *) NULL;
        memory_initialized = true;
    }

    if (size == 0)
        return NULL;

    struct mblock **iter = &head;
    while (*iter) {
        if ((*iter)->size >= size) {
            // Large enough free memory block found

            struct mblock *next_free_block = (*iter)->next;
            if ((*iter)->size > size + MBLOCK_SIZE) {
                // Enough free space to create a new free block
                struct mblock *new_free_block = (struct mblock *) ((*iter)->memory + size);
                new_free_block->size = (*iter)->size - size - MBLOCK_SIZE;
                new_free_block->next = (*iter)->next;
                (*iter)->size = size;
                next_free_block = new_free_block;
            }

            // Mark this block as allocated
            (*iter)->next = (struct mblock *) MAGIC;
            void *mem = (*iter)->memory;
            *iter = next_free_block;

            // Return pointer to allocated memory
            return mem;
        }
        // Iterate to the next free memory block
        iter = &(*iter)->next;
    }

    // There were no large enough free memory blocks
    errno = ENOMEM;
    return NULL;
}

void free (void *ptr) {
    if (ptr == NULL)
        return;

    struct mblock *block = mem2block(ptr);

    // The next pointer for an allocated block should be the magic number
    if (block->next != MAGIC)
        abort();

    // Add this block at the front of the free block list
    block->next = head;
    head = block;
}

void *realloc (void *ptr, size_t size) {
    if (size == 0) {
        free(ptr);
        return NULL;
    }

    // Allocate new memory
    void *new_mem = malloc(size);
    if (!new_mem)
        return NULL;

    if (ptr) {
        // Copy contents of old memory into new memory
        memcpy(new_mem, ptr, mem2block(ptr)->size);
        free(ptr);
    }

    return new_mem;
}

void *calloc (size_t nmemb, size_t size) {
    assert((nmemb * size) <= SIZE_MAX && (nmemb * size) >= 0);

    size_t total_size = nmemb * size;

    void *mem = malloc(total_size);

    if (!mem)
        return NULL;

    return memset(mem, 0, total_size);
}

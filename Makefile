.PHONY: all clean

CC  := gcc
RM  := rm

CPPFLAGS := -D_XOPEN_SOURCE=700
CFLAGS   := -std=c11 -pedantic -Wall -Werror
LDFLAGS  :=
LDLIBS   :=

OUT  := test test-ref
SRC  := $(wildcard *.c)
BASE := $(basename $(SRC))
OBJ  := $(addsuffix .o,$(BASE))

all: test test-ref

test: test.o halde.o
	$(CC) -o $@ $^ $(LDFLAGS) $(LDLIBS)

test-ref: test.o halde-ref.o
	$(CC) -o $@ $^ $(LDFLAGS) $(LDLIBS)

%.o: %.c
	$(CC) -o $@ $< -c $(CPPFLAGS) $(CFLAGS)

clean:
	$(RM) -f $(OUT) $(OBJ)
